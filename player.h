#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <vector>
#include <map>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    Move *minimaxAlg(Board * board, int plies, Side side, Side original);
    Move *alphabetaAlg(Board * board, int plies, Side side, double alpha, double beta);
    vector<Move *> allMoves(Board * board, Side side);
    
    double heuristic_score(Board * board, Side side, Move * move);
    
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    
    Side ourside;
    Side otherside;

    Board board;

};

#endif
