#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    ourside = side;
    otherside = (ourside == WHITE) ?  BLACK : WHITE;

}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {

    // Record the opponent's move
    board.doMove(opponentsMove, otherside);

    // If no legal moves, can return NULL
    if (!board.hasMoves(ourside)) {
        return NULL;

    } else 
    {
        Move * move;
        Board * copy = board.copy();

        // If testing the minimax algorithm, use the 2-ply naive heuristic approach
        if (testingMinimax) {
            move = minimaxAlg(copy, 2, ourside, ourside);

        // Otherwise, do current best alg: alphabetaAlg ply = 6
        } else {
            move = alphabetaAlg(copy, 2, ourside, -1000, 1000);
        }
            
        cerr << "\n Chose move (" << move->x << ", " << move->y << ").\n" << endl;

        board.doMove(move, ourside);
        return move;
    }
}


Move *Player::minimaxAlg(Board * board, int plies, Side side, Side original) {
    Move *final_move = NULL, *poss_move;
    Board *simboard = board->copy();
    int score, final_score = (side == original) ? -1000 : 1000;
    vector<Move *> possible_moves;

    if (plies <= 0) {
        return NULL;
    }

    // If no legal moves, can return NULL
    if (!board->hasMoves(side)) {
        return NULL; 
    }

    // Get all moves.
    possible_moves = allMoves(board, side);

    // Debugging
    // for(vector<Move *>::iterator it=possible_moves.begin(); it!=possible_moves.end(); ++it) {
    //     cerr << (*it)->x << ", " << (*it)->y << "  ";
    // }

    // cerr << endl;


    // Go over each move.
    for (vector<Move *>::iterator it=possible_moves.begin(); it!=possible_moves.end(); ++it) {

        simboard->doMove(*it, side);

        // If on last ply, find the score of doing that move.
        if (plies == 1) {
            score = simboard->count(original) - simboard->count((original == BLACK) ? WHITE : BLACK);

        // Otherwise, simulate more moves.
        } else {
            poss_move = minimaxAlg(simboard, plies - 1, (side == BLACK) ? WHITE : BLACK, original);
            simboard->doMove(poss_move, (side == BLACK) ? WHITE : BLACK);
            score = simboard->count(original) - simboard->count((original == BLACK) ? WHITE : BLACK);
        }

        delete simboard;
        simboard = board->copy();

        // Store greatest or smallest score depending on the side 
        if (side == original) {
            if (score > final_score) {
                final_move = *it;
                final_score  = score;
            }
        } else {
            if (score < final_score) {
                final_move = *it;
                final_score  = score;
            }
        }
    }

    return final_move;
}


//**********************************************************************
Move *Player::alphabetaAlg(Board * board, int plies, Side side, double alpha, double beta) 
{
    Move *final_move = NULL, *poss_move;
    Board *simboard = board->copy();
    double score, final_score = (side == ourside) ? -1000 : 1000;
    vector<Move *> possible_moves;

    if (plies <= 0) {
        return NULL;
    }

    // If no legal moves, can return NULL
    if (!board->hasMoves(side)) {
        return NULL; 
    }

    // Get all moves.
    possible_moves = allMoves(board, side);

    // Debugging
    // for(vector<Move *>::iterator it=possible_moves.begin(); it!=possible_moves.end(); ++it) {
    //     cerr << (*it)->x << ", " << (*it)->y << "  ";
    // }

    // cerr << endl;


    // Go over each move and find the alpha/beta scores for that move.
    // If beta > alpha, disregard that tree because we dun goofed.
    for (vector<Move *>::iterator it=possible_moves.begin(); it!=possible_moves.end(); ++it) {

        

        // If on last ply, find the score of doing that move.
        if (plies == 1) 
        {
            score = heuristic_score(simboard, side, *it);

            // Debugging
            //cerr << "For the 1move (" << (*it)->x << ", " << (*it)->y << "), score is " << score << ". " << endl;


            // Store greatest or smallest score depending on the side 
            if (side == ourside) 
            {
                if (score > final_score) 
                {
                    final_score = score;
                    final_move = *it;
                }
                if (final_score > alpha) 
                {                   
                    alpha  = final_score;
                }
            } else {
                score = score * -1;
                if (score < final_score) 
                {
                    final_score = score;
                    final_move = *it;
                }
                if (final_score < beta) 
                {
                    beta  = final_score;
                }
            }
            
            // If alpha becomes greater than beta, stop going down the tree
            if (alpha > beta)
            {
				return final_move;
			}

        }
        // Otherwise, simulate more moves.
        else 
        {
			simboard->doMove(*it, side);
            poss_move = alphabetaAlg(simboard, plies - 1, (side == ourside) ? otherside : ourside, alpha, beta);
            // simboard->doMove(poss_move, (side == ourside) ? otherside : ourside);
            if (poss_move) {
                score = heuristic_score(simboard, (side == ourside) ? otherside : ourside, poss_move);

                // Debugging
                //cerr << "For the 2move (" << poss_move->x << ", " << poss_move->y << "), score is " << score << ". ";

                

            } else {
                score = heuristic_score(board, side, *it);

                // Debugging
                //cerr << "For the 3move (" << poss_move->x << ", " << poss_move->y << "), score is " << score << ". ";
            }

            // Store greatest or smallest score depending on the side 
            if (side == ourside) 
            {
                score = score * -1;
                if (score > final_score) 
                {
                    final_score = score;
                    final_move = *it;
                }
                if (final_score > alpha) 
                {                   
                    alpha  = final_score;
                }
            } else {
                if (score < final_score) 
                {
                    final_score = score;
                    final_move = *it;
                }
                if (final_score < beta) 
                {
                    beta  = final_score;
                }
            }
            
            // If alpha becomes greater than beta, stop going down the tree
            if (alpha > beta)
            {
                return final_move;
            }
			
        }

        delete simboard;
        simboard = board->copy();

        score = 0;

  
    }

    delete simboard;

    //cerr << "final score is " << final_score << endl;

    return final_move;
}

// Outputs the alpha/beta scores of a certain move depending on the 
// strategies given. Assume all inputted moves are legal.

double Player::heuristic_score(Board * board, Side side, Move * move)
{
	double alpha = 0;

    //cerr << "Move is  (" << move->x << ", " << move->y << ") " << endl;
    
	
// Always take corners. Especially take a corner if it is across from
// a corner you/the opponent already owns.
	if(move->x == 0 && move->y == 0)
	{

        //cerr << "Considering corners 1... " ;

		alpha += 10;
		// If opposite corners are taken, counts as more than diagonal/alone
		if(board->occupied(0, 7))
		{
			alpha += 4;
			// if both opposite corners are taken, very valuable
			if(board->occupied(7, 0))
			{
				alpha += 6;
				// if all other corners are taken, even more valuable
				if(board-> occupied(7, 7))
				{
					alpha += 8;
				}
			}
			// if opposite & diagonal are taken, very valuable
			else if(board->occupied(7, 7))
			{
				alpha += 5;
			}
		}
		// If 1 opposite and possibly diagonal are taken...
		else if(board->occupied(7, 0))
		{
			alpha += 4;
			// if opposite & diagonal are taken, very valuable
			if(board->occupied(7, 7))
			{
				alpha += 5;
			}
		}
		// If only diagonal is taken, slightly more valuable
		else if(board->occupied(7, 7))
		{
			alpha += 3;
		}

        //cerr << "Done considering. " << endl;
	}
	// Repeat for other 3 corners... this part is super long but I
	// could not figure out how to shorten it without another function.
	if(move->x == 0 && move->y == 7)
	{

        // cerr << "Considering corners 2... " ;


		alpha += 10;
		// If opposite corners are taken, counts as more than diagonal/alone
		if(board->occupied(0, 0))
		{
			alpha += 4;
			// if both opposite corners are taken, very valuable
			if(board->occupied(7, 7))
			{
				alpha += 6;
				// if all other corners are taken, even more valuable
				if(board-> occupied(7, 0))
				{
					alpha += 8;
				}
			}
			// if opposite & diagonal are taken, very valuable
			else if(board->occupied(7, 0))
			{
				alpha += 5;
			}
		}
		// If 1 opposite and possibly diagonal are taken...
		else if(board->occupied(7, 7))
		{
			alpha += 4;
			// if opposite & diagonal are taken, very valuable
			if(board->occupied(7, 0))
			{
				alpha += 5;
			}
		}
		// If only diagonal is taken, slightly more valuable
		else if(board->occupied(7, 0))
		{
			alpha += 3;
		}

        //cerr << "Done considering. " << endl;
	}
	if(move->x == 7 && move->y == 0)
	{

       // cerr << "Considering corners 3... " ;

		alpha += 10;
		// If opposite corners are taken, counts as more than diagonal/alone
		if(board->occupied(7, 0))
		{
			alpha += 4;
			// if both opposite corners are taken, very valuable
			if(board->occupied(7, 7))
			{
				alpha += 6;
				// if all other corners are taken, even more valuable
				if(board-> occupied(0, 7))
				{
					alpha += 8;
				}
			}
			// if opposite & diagonal are taken, very valuable
			else if(board->occupied(0, 7))
			{
				alpha += 5;
			}
		}
		// If 1 opposite and possibly diagonal are taken...
		else if(board->occupied(7, 7))
		{
			alpha += 4;
			// if opposite & diagonal are taken, very valuable
			if(board->occupied(0, 7))
			{
				alpha += 5;
			}
		}
		// If only diagonal is taken, slightly more valuable
		else if(board->occupied(0, 7))
		{
			alpha += 3;
		}

        //cerr << "Done considering. " << endl;
	}
	if(move->x == 7 && move->y == 7)
	{

      //  cerr << "Considering corners 4... " ;

		alpha += 10;
		// If opposite corners are taken, counts as more than diagonal/alone
		if(board->occupied(0, 7))
		{
			alpha += 4;
			// if both opposite corners are taken, very valuable
			if(board->occupied(7, 0))
			{
				alpha += 6;
				// if all other corners are taken, even more valuable
				if(board-> occupied(0, 0))
				{
					alpha += 8;
				}
			}
			// if opposite & diagonal are taken, very valuable
			else if(board->occupied(0, 0))
			{
				alpha += 5;
			}
		}
		// If 1 opposite and possibly diagonal are taken...
		else if(board->occupied(7, 0))
		{
			alpha += 4;
			// if opposite & diagonal are taken, very valuable
			if(board->occupied(0, 0))
			{
				alpha += 5;
			}
		}
		// If only diagonal is taken, slightly more valuable
		else if(board->occupied(0, 0))
		{
			alpha += 3;
		}

       //cerr << "Done considering. " << endl;
	}

	alpha = alpha * 4;

 //cerr << "Considering nexttocorners... " ;


	if ((move->x == 0) && (move->y == 1 || move->y == 6))
	{
		alpha += 15;
	}
	else if ((move->x == 7) && (move->y == 1 || move->y == 6))
	{
		alpha += 15;
	}
	else if(move->x == 1 && (move->y == 0 || move->y == 1 || move->y == 6 || move->y == 7))
	{
		alpha += 15;
	}
	else if(move->x == 6 && (move->y == 0 || move->y == 1 || move->y == 6 || move->y == 7))
	{
		alpha += 15;
	}

    //cerr << "Done considering. " << endl;

// "Frontiers" are bad. Moves surrounded by other blocks are better.
// Must deal with edge cases here to avoid whatever happens when
// accessing a nonexistent move.
	double frontier;
	if(move->x > 0 && move->x < 7)
	{

        //cerr << "Considering frontiers... " ;

		if(move->y > 0 && move->y < 7)
		{
			for(int i = -1; i <= 1; i++)
			{
				for(int j = -1; j <= 1; j++)
				{
					if(board->occupied(move->x + i, move->y + j))
					{
						frontier += 1;
					}
				}
			}
		}

        //cerr << "Done considering. " << endl;
	}
// Different thresholds for frontier
	if(frontier > 4 && frontier < 6)
	{
		alpha += 10;
	}
	else if (frontier >= 6)
	{
		alpha += 20;
	}		
	
// As there are fewer spaces left, the moves become more important.
// The last move should be the most important.
// Call minimax to block the opponent's better moves later in the game.


	int total = board->countBlack() +  board->countWhite();
    Board * simboard = board->copy();
// This only kicks in if there are a certain number of moves already made
	if((total >= 50) & simboard->hasMoves((side == BLACK) ? WHITE : BLACK));
	{
        Move * poss_move = minimaxAlg(simboard, 2, (side == BLACK) ? WHITE : BLACK, (side == BLACK) ? WHITE : BLACK);
        
        if (poss_move) {
            //cerr << "Considering finalmoves... " ;

            // Check for the opponent's best move
            // Use minimaxAlg to prevent infinite recursion

            //cerr << "still considering... ";

            // cerr << "Poss_move is (" << poss_move->getX() << ", " << poss_move->getY() << "), our move is (" 
            //      << move->x << ", " << move->y << ") " << endl;

            // If opponent's best move is the one we are considering, we must take it.
            if ((move->x == poss_move->x) && (move->y == poss_move->y)) {
                alpha += 100;
                // Double the score if it's in the last 10 moves.
                if (total > 57) alpha = alpha * 2;
            }


            
    		// If an opponent's move gets a higher score for them, we should
    		// make that move instead of letting them have it.
    		
    		//cerr << "Done considering. " << endl;

        }   
	}



    //cerr << "Last steps... " ;

    //cerr << "Move is  (" << move->x << ", " << move->y << ") " << endl;
    
    simboard->doMove(move, side);

    // Consider Mobility
    vector<Move *> ourmoves = allMoves(simboard, side);
    vector<Move *> oppmoves = allMoves(simboard, (side == BLACK) ? WHITE : BLACK);

    // cerr << "Sizes are " << ourmoves.size() << ", " << oppmoves.size() << endl;

    alpha += (double) (150 * ((double) ourmoves.size() - (double) oppmoves.size()) / ((double) ourmoves.size() + (double) oppmoves.size()));


    // Last case if nothing else applies.
    alpha += simboard->count(side) - simboard->count((side == BLACK) ? WHITE : BLACK);

    

    delete simboard;

    //cerr << "Done." << endl;
    


	
	return alpha;
}
//**********************************************************************

/*
* Finds all possible legal moves for Player's side of the board.
*/
vector<Move *> Player::allMoves(Board * board, Side side) {
    Move * move;
    vector<Move *> possible_moves;

    // Get all moves.
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {

            move = new Move(i, j);
            if (board->checkMove(move, side)) {
                possible_moves.push_back(move);                
            } else {
                delete move;
            } 
        }
    }

    return possible_moves;
}

