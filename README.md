CS 2 Othello Project
====================

by Bhavana Jonnalagadda and Liz Beaver


### Contributions

- Liz Beaver: Implemented the alpha-beta algorithm function. Researched and found optimal heuristic guidelines. Implemented the heuristic_score function (which calculates the score based on included heuristic functions).

- Bhavana J: Implemented the do_move() function in Player. Implemented the minimax algorithm and allMoves(). Worked on debugging final code.


### Improvements Made

We decided that a 4-ply alpha-beta pruning algorithm was a good tree algorithm in which to implement different heuristic functions. The heursitic function works for both our side and the opponent's side, where the opponent score is made negative; thus, the  function is based on symmetrical mini/max scoring. We added these types:

- Corner optimizing: the function highly rates moves that take corners.
- Near-corner optimizing: the function slightly prefers moves near corners.
- Flanking/minimizing frontiers: the less empty spaces around a move, the higher rated the move is.
- Increased importance for fuller boards: after 50 spaces have been filled, increase rating of moves; increase even more for over 57 spots filled.
- Taking moves from the opponent: after 50 spots have been filled on the board, if the move being considered is the one the opponent would take, take that move.
- Mobility: Rate higher moves that tend to give more available moves to us than the opponent, after the considered move is taken.
- Piece Difference: slightly consider the difference in pieces on the board. Important when no other heuristics apply.

All of the above heuristics were added because the have been shown to be solid othello strategies used by human players. We believe our strategy will work because the common literature accepts the alpha-beta algorithm as a solid one when enough clever heuristics are hard-coded into it. 
Briefly, an evolutionary artifical neural network (EANN) was considered, but the amount of errors generated and time required for the EANN to train were too big of deterrents.